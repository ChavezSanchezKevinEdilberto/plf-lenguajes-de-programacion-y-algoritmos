# TEMA: "Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)"
```plantuml
@startmindmap
*[#Orange] Programando ordenadores \nen los 80 y ahora
	*[#Pink] Sistemas antiguos
		*[#98FB98] Conjunto de pasos orientados a la resolución de un problema
		*[#98FB98] Ordenadores de la decada \n de los 80's y 90's
		*[#98FB98] La programación en los 80's o 90's
			*[#DDA0DD] Poder realizar la programación en diferentes maquinas, \nera necesario conocer la arquitectura de hardware de\ncada máquina
			*[#DDA0DD] Cuellos de botellas
			*[#DDA0DD] Limitaciones
	*[#Pink] Sistemas actuales
		*[#98FB98]_ Se encuentran en 
			*[#98FB98] Casa
			*_ o se puede adquirir en alguna
				*[#98FB98] Tienda
		*[#98FB98] Su funcionamiento, gradualmente irá en decaida
	*[#Pink] Novedades y diferencias de la actualidad
		*[#98FB98] Nuevas prestaciones
 		*[#DDA0DD] Eficiencia en la vida cotidiana
 		*[#DDA0DD] Precios accesibles
		*[#98FB98] La velocidad del procesador 
			*[#98FB98]_ Se media en 
				*[#98FB98] MHz
		*[#98FB98] Arquitecturas
	*[#Pink] Lenguajes
		*[#98FB98] Son de ayuda para la comunicación entre la máquina y el programador
		*[#lightblue] Alto nivel
			*[#98FB98] Traducción a código máquina
			*[#98FB98] Actualmente lo realizan los compiladores e intérpretes
			*[#98FB98] Desventajas
				*[#DDA0DD] Sobrecargo para las capas realizadas
				*[#DDA0DD] Los compiladores no son tan eficientes como los que se generan en ensamblador
				*[#DDA0DD] Muchas veces los programadores desconocen el funcionamiento del hardware
			*_ Ejemplo:
				*[#DDA0DD] JAVA
					*[#B0C4DE] No se compila directamente a la máquina, se ejecuta
					*[#B0C4DE] Unity 
						*[#FFFAFA] Permite generar programas que funcionen \nde la misma forma para todas las plataformas
		*[#lightblue] Bajo nivel
			*[#98FB98] Lo realizaba el propio programador
			*[#98FB98] Lenguaje ensamblador
				*[#DDA0DD] Aprovechas recursos
				*[#DDA0DD] Control total sobre la máquina
	*[#Pink] Leyes
		*[#lightblue] Moore
			*[#98FB98] Cada 18 meses se pueden generar una CPU el doble de rápido
		*[#lightblue] Page
			*[#98FB98] Cada 18 meses el software se vuelve el doble de lento
@endmindmap
```
# TEMA: "Hª de los algoritmos y de los lenguajes de programación (2010)"
```plantuml
@startmindmap
*[#Orange] Historia de los algoritmos y de \nlos lenguajes de programación
	*[#Pink] Algoritmo
		*[#98FB98] Conjunto de pasos orientados a la resolución de un problema
		*[#98FB98] Características:
			*_ Debe ser
				*[#B0C4DE] Una secuencia ordenada
				*[#B0C4DE] Finita 
				*[#B0C4DE] Definida de instrucciones
			*[#DDA0DD] Mostrar la manera de llevar a cabo procesos 
			*[#DDA0DD] Resolver mecánicamente problemas matemáticos o de otro tipo
		*_ Ejemplo:
			*[#DDA0DD] Manuales de usuario
			*[#DDA0DD] Recetas de cocina
			*[#DDA0DD] Etcétera
		*[#98FB98] Problemas indecibles
			*[#DDA0DD] Problemas complejos de lo que los algoritmos \npueden o podrán en su día resolver
				*_ Ejemplo:
					*[#FFFAFA] Saber a la vista de un programa si ese programa terminará o no
					*[#FFFAFA] Saber cuánta memoria va a consumir o cuánto tiempo tardará en ejecutarse
		*_ Se dividen en
			*[#DDA0DD] Razonables
				*[#B0C4DE] Son algoritmos cuyo tiempo de ejecución crece despacio \na medida que los problemas se van haciendo más grandes 
				*_ Ejemplo:
					*[#FFFAFA] Multiplicar dos números de muchas cifras, se considera que es un problema razonable
			*[#DDA0DD] No razonables
				*[#B0C4DE] Se le llama exponenciales o super polinomiales son aquellos que se comportan muy mal
				*_ Ejemplo:
					*[#FFFAFA] Añadir un dato más al problema hace que se duplique el tiempo
		*[#98FB98] Historia
			*_ Empleado en
				*[#DDA0DD] Mesopotamia hace 3000 años 
					*[#B0C4DE] Cálculos relacionados con transacciones comerciales
			*[#DDA0DD] En el Siglo XVII 
				*[#B0C4DE] Primeras ayudas mecánicas 
					*[#FFFAFA] Calculadoras de sobremesa 
			*[#DDA0DD] En el Siglo XIX 
				*[#B0C4DE] Primeras máquinas programables
	*[#Pink] Programa
		*[#98FB98] Conjunto de operaciones especificadas en un determinado \nlenguaje de programación y para un computador concreto \nsusceptible de ser ejecutado, compilado o interpretado
			*[#DDA0DD] Empleado en 1800
				*[#B0C4DE] Por medio de telares
			*[#DDA0DD] En el siglo XIX 
				*[#B0C4DE] Aparece la piola
	*[#Pink] Máquinas programables
		*[#98FB98] Aparecen el siglo XVII 
			*[#DDA0DD] Calculadoras
				*[#B0C4DE] Mecánicas 
				*[#B0C4DE] No programables
		*[#98FB98] Primer computadora
			*[#DDA0DD] Máquina analítica
				*_ Diseñado por
					*[#B0C4DE] Charles babbage
						*[#FFFAFA] Siglo XIX
				*[#FFFAFA] Operaciones básicas
					*[#F08080] Suma
					*[#F08080] Resta
					*[#F08080] Multiplicación
					*[#F08080] División
				*[#FFFAFA] El programa estaba en unas tablas de madera \nperforadas y esas eran las que dirigían los cálculos
	*[#Pink] Lenguajes de programación
		*[#98FB98] Instrumentos apropiados para comunicar los \nalgoritmos a las máquinas que han de ejecutarlos
		*[#98FB98] Paradigmas o familias de lenguaje
			*[#DDA0DD] Paradigma funcional
				*[#B0C4DE] Trabajan un poco con esta idea de la simplificación
				*[#B0C4DE] En los 60
					*_ surge
						*[#FFFAFA] LISP
				*[#B0C4DE] Otros lenguajes 
					*[#FFFAFA] Erlang
					*[#FFFAFA] Haskell
			*[#DDA0DD] Lenguajes orientados a objetos
				*[#B0C4DE] En el 68 
					*_ surge
						*[#FFFAFA] SIMULA
				*[#B0C4DE] Otros lenguajes 
					*[#FFFAFA] JAVA
			*[#DDA0DD] Programación lógica
				*[#B0C4DE]_ se usan en 
					*[#B0C4DE] Empresas que tienen problemas de optimización
				*[#B0C4DE] En el mundo de la inteligencia artificial
				*[#B0C4DE] En el 71
					*_ Aparece 
						*[#FFFAFA] Prolog
		*[#98FB98] 	Lenguajes influyentes
			*[#DDA0DD] FORTRAN
				*[#B0C4DE]_ Surge en 
					*[#B0C4DE] 1959 
						*[#FFFAFA] Primer lenguaje de alto nivel
			*[#DDA0DD] JAVA
				*[#B0C4DE] Del año 1995
			*[#DDA0DD] C++ 
				*[#B0C4DE] De los 80
@endmindmap
```
# TEMA: "Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)"
```plantuml
@startmindmap
*[#Orange] La evolución de los lenguajes \ny paradigmas de programación
	*[#Pink] El ordenador
		*_ Crado para
			*[#98FB98] Crado para facilitar el trabajo intelectual 
	*[#Pink] Lenguajes de programación
		*[#98FB98] Siempre innovar para resolver problemas diversos y complejos, \nfacilitando la eficiencia de ejecución
	*[#Pink] Las máquinas:
		*[#98FB98] Codificado en lenguaje binario
		*_ Se usan 
			*[#98FB98] Acronimos
				*_ en
					*[#98FB98] Lenguaje ensamblador
	*[#Pink] Programación estructurada:
		*_ Consiste en 
			*[#98FB98] Analizar una función principal
				*[#DDA0DD] Descomponer a funciones más sencillas de resolver
		*_ Ejemplos:
			*[#98FB98] Basic
			*[#98FB98] C
			*[#98FB98] Pascal
			*[#98FB98] Fortran
			*[#98FB98] JAVA
				*[#DDA0DD] Usado para la programación orientada a objetos
				*[#DDA0DD] Organizar el código de un programa
				*[#DDA0DD] El cliente cambia sus necesidades, el mercado cambia
				*[#DDA0DD] Permite algoritmos de díalogo entre los objetos
				*[#DDA0DD] Los objetos
					*_ ofrecen una forma de pensar
						*[#B0C4DE] Más abstracta
	*[#Pink] Paradigma:
		*[#98FB98] Es una forma de pensar o abordar el desarrollo de un programa
		*[#98FB98] ¿Por qué aparecen?
			*[#DDA0DD] Su creación se da por las fallas de la programación estructurada se crean otros paradigmas para corregir
		*[#98FB98] Tipos
			*[#lightblue] Funcional
				*[#DDA0DD] Los programas son correctos (verídicos)
					*_ funciones matemáticas que devuelven 
						*[#B0C4DE] un número o letra
					*[#B0C4DE] Uso de recursividad hasta que se cumpla cierta condición
				*_ Ejemplos de lenguajes:
					*[#B0C4DE] ML
					*[#B0C4DE] HOPE
					*[#B0C4DE] HASKELL
					*[#B0C4DE] ERLANG
			*[#lightblue] Lógico
				*[#DDA0DD] Lenguaje de la lógica
					*[#B0C4DE] Predicados lógicos 
						*[#FFFAFA] A traves de un hecho
							*[#F08080] Se aplican las reglas 
							*[#F08080] El programa determina si es cierto o falso
					*[#B0C4DE] Uso de recursividad
					*_ No devuelve
						*[#FFFAFA] Números
						*[#FFFAFA] Letras
						*[#FFFAFA] Operaciones arítmeticas  
					*_ Ejemplo de lenguaje
						*[#FFFAFA] Prolog
				
			*[#lightblue] Programación Concurrente
				*[#DDA0DD] Sistemas automáticos
					*[#B0C4DE] Cierta cantidad de usuarios busca acceder a un sitio
					*[#B0C4DE] Se programan políticas de sincronización
					*_ Ejemplo:
						*[#FFFAFA] Una cuenta vacía no puede sacar dinero 
						*[#FFFAFA] Una cuenta que hace una transacción no puede realizar otra simultáneamente  
			*[#lightblue] Programación distribuida
				*[#DDA0DD] Comunicación entre los ordenadores
				*[#DDA0DD] Programas pesados divididos en diversos ordenadores
				*_ Ejemplo: 
					*[#B0C4DE] Proyecto SETI
						*[#FFFAFA] Se llevó a cabo por medio de internet para la resolución de problemas complejos
			*[#lightblue] Programación orientada en componentes
				*[#DDA0DD] Un componente es un conjunto de objetos
				*[#DDA0DD] Permite la reutilización
				*_ Ejemplo:
					*[#B0C4DE] Un motor de un coche contiene piezas aún más pequeñas
			*[#lightblue] Programación orientada en aspectos
				*[#DDA0DD] Al implementar el esqueleto del programa, \nagregar capas en base a la necesidad del programa
				*[#DDA0DD] Las capas deben mantenerse independiente, \npermitiendo la legibilidad
					*_ permitiendo 
						*[#B0C4DE] Legibilidad
						*[#B0C4DE] Comodidad
			*[#lightblue] Programación orientada agente software 
				*_ Forman parte de la
					*[#DDA0DD] Programacion orientada a objetos
						*_ Son
							*[#B0C4DE] Entidades autónomas con el propósito de alcanzar sus objetivos
						*_ Los 
							*[#B0C4DE] Multiagentes 
								*[#FFFAFA] Resolver grandes problemas
								*[#FFFAFA] Dialogar
								*[#FFFAFA] Cooperar
								*[#FFFAFA] Coordinar
								*[#FFFAFA] Negociación
				*_ Ejemplo:
					*[#DDA0DD] El agente busca recomendaciones con base en los gustos del perfil del cliente
@endmindmap
```
## Autor: Chávez Sánchez Kevin Edilberto

[ChavezSanchezKevinEdilberto @ GitLab](https://gitlab.com/ChavezSanchezKevinEdilberto)

### Para la creación de estos Para la creación de tus mapas mentales harás uso de los formatos Markdown y PlantUML.
#### Herramientas
#### Markdown
[markdown ](https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts)
#### PlantUML
[plantuml ](https://docs.gitlab.com/ee/administration/integration/plantuml.html)
#### MindMap
[mindmap ](https://plantuml.com/es/mindmap-diagram)